package com.example.testproject.utils

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log

fun checkInternetConnection(context: Context): Boolean {
    if (!isOnline(context)) {
        showNoInternetDialog(context)
        return false
    }
    return true
}

private fun isOnline(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val capabilities =
        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    if (capabilities != null) {
        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
            Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
            return true
        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
            Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
            return true
        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
            Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
            return true
        }
    }
    return false
}

private fun showNoInternetDialog(context: Context) {
    AlertDialog.Builder(context)
        .setTitle("No Internet connection")
        .setMessage("Check your connection and try again")
        .setPositiveButton("Reload") { dialogInterface, _ ->
            dialogInterface.dismiss()
            checkInternetConnection(context)
        }
        .create()
        .show()
}
