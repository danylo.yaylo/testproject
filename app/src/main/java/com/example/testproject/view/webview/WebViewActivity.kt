package com.example.testproject.view.webview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.navigation.findNavController
import androidx.navigation.navArgs
import com.example.testproject.R
import com.example.testproject.utils.DEFAULT_URL
import com.example.testproject.utils.URL_TAG
import com.example.testproject.utils.checkInternetConnection

class WebViewActivity : AppCompatActivity() {
    private var webView: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        webView = WebView(this)
        setContentView(webView)
        checkInternetConnection(this)
        parseUrl()
    }

    private fun parseUrl() {
        val url = (intent.extras?.getString(URL_TAG, "") ?: "")
            .ifEmpty { DEFAULT_URL }

        webView?.webViewClient = WebViewClient()
        webView?.settings?.setSupportZoom(true)
        webView?.loadUrl(url)

    }

    override fun onBackPressed() {
        if (webView?.canGoBack() == true)
            webView?.goBack()
        else
            super.onBackPressed()
    }
}
