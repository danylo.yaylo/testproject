package com.example.testproject.view.search

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment
import com.example.testproject.R
import com.example.testproject.utils.DEFAULT_URL
import com.example.testproject.utils.URL_TAG
import com.example.testproject.utils.checkInternetConnection
import com.example.testproject.view.custom_webview.CustomWebViewActivity
import com.example.testproject.view.webview.WebViewActivity

class SearchFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupButtons()
    }

    private fun setupButtons() {
        val webViewButton = requireView().findViewById<Button>(R.id.webview_button)
        val customWebViewButton = requireView().findViewById<Button>(R.id.custom_webview_button)
        val chromeTabsButton = requireView().findViewById<Button>(R.id.chrome_tabs_button)

        webViewButton.setOnClickListener {
            if (checkInternetConnection(requireContext())) {
                val intent = Intent(requireActivity(), WebViewActivity::class.java)
                intent.putExtra(URL_TAG, getUrl())
                requireActivity().startActivity(intent)
            }
        }
        customWebViewButton.setOnClickListener {
            if (checkInternetConnection(requireContext())) {
                val intent = Intent(requireActivity(), CustomWebViewActivity::class.java)
                intent.putExtra(URL_TAG, getUrl())
                requireActivity().startActivity(intent)
            }
        }
        chromeTabsButton.setOnClickListener {
            if (checkInternetConnection(requireContext())) {
                val customTabsIntent: CustomTabsIntent = CustomTabsIntent.Builder().build()
                customTabsIntent.launchUrl(
                    requireContext(),
                    Uri.parse(getUrl().ifEmpty { DEFAULT_URL })
                )
            }
        }
    }

    private fun getUrl(): String {
        val inputLayout = requireView().findViewById<EditText>(R.id.url_input_field)
        return inputLayout.text.toString()
    }

}
