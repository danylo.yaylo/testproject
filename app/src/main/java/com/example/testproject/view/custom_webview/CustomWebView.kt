package com.example.testproject.view.custom_webview

import android.content.Context
import android.webkit.WebView
import com.example.testproject.utils.checkInternetConnection


class CustomWebView(context: Context): WebView(context) {
    init {
        this.settings.useWideViewPort = true
        this.settings.loadWithOverviewMode = true
        this.settings.domStorageEnabled = true
        checkInternetConnection(context)
    }
}
